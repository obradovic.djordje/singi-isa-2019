package rs.ac.singidunum.banka.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

//@Entity
//@DiscriminatorValue("Devizni")
public class DevizniRacun extends Racun {
	private String valuta;
	
	public DevizniRacun() {
		// TODO Auto-generated constructor stub
	}

	public DevizniRacun(String valuta) {
		super();
		this.valuta = valuta;
	}

	public String getValuta() {
		return valuta;
	}

	public void setValuta(String valuta) {
		this.valuta = valuta;
	}
}
