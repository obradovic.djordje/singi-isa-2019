package rs.ac.singidunum.banka.model;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

//@Entity
//@DiscriminatorValue("Stedni")
public class StedniRacun extends Racun {
	@Temporal(TemporalType.DATE)
	private Date orocenje;
	
	public StedniRacun() {
		
	}

	public StedniRacun(Date orocenje) {
		super();
		this.orocenje = orocenje;
	}

	public Date getOrocenje() {
		return orocenje;
	}

	public void setOrocenje(Date orocenje) {
		this.orocenje = orocenje;
	}
	
	
}
