package rs.ac.singidunum.banka.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonView;

import rs.ac.singidunum.banka.utils.View.HideOptionalProperties;

@Entity
//@Inheritance(strategy=InheritanceType.JOINED)
//@DiscriminatorColumn(name="tip_racuna", discriminatorType=DiscriminatorType.STRING)
//@DiscriminatorValue("Obican")
public class Racun {
	public interface ShowTransakcije {};
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(unique=true, length=128)
	private String brojRacuna;
	
	private double stanje;
	
	@OneToMany(mappedBy="posiljalac")
	@JsonView(ShowTransakcije.class)
	private Set<Transakcija> transakcijePosiljalac;
	
	@OneToMany(mappedBy="primalac")
	@JsonView(ShowTransakcije.class)
	private Set<Transakcija> transakcijePrimalac;
	
	public Racun() {
	}

	public Racun(Long id, String brojRacuna, double stanje) {
		super();
		this.id = id;
		this.brojRacuna = brojRacuna;
		this.stanje = stanje;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBrojRacuna() {
		return brojRacuna;
	}

	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}

	public double getStanje() {
		return stanje;
	}

	public void setStanje(double stanje) {
		this.stanje = stanje;
	}

	public Set<Transakcija> getTransakcijePosiljalac() {
		return transakcijePosiljalac;
	}

	public void setTransakcijePosiljalac(Set<Transakcija> transakcijePosiljalac) {
		this.transakcijePosiljalac = transakcijePosiljalac;
	}

	public Set<Transakcija> getTransakcijePrimalac() {
		return transakcijePrimalac;
	}

	public void setTransakcijePrimalac(Set<Transakcija> transakcijePrimalac) {
		this.transakcijePrimalac = transakcijePrimalac;
	}
}
