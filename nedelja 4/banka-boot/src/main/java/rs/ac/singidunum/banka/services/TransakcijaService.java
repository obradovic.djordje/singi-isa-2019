package rs.ac.singidunum.banka.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.banka.model.Racun;
import rs.ac.singidunum.banka.model.Transakcija;
import rs.ac.singidunum.banka.repositories.RacunRepository;
import rs.ac.singidunum.banka.repositories.TransakcijaRepository;

@Service
public class TransakcijaService {
	@Autowired
	RacunRepository rr;
	
	@Autowired
	TransakcijaRepository tr;

	public TransakcijaService() {
	}

	public Iterable<Transakcija> getTransakcije() {
		return tr.findAll();
	}

	public Optional<Transakcija> getTransakcija(Long id) {
		return tr.findById(id);
	}

	public Transakcija addTransakcija(String brojRacunaPosiljaoca, String brojRacunaPrimaoca, double iznos) {
		Optional<Racun> racunPrimaoca = rr.findByBrojRacuna(brojRacunaPrimaoca);
		Optional<Racun> racunPosiljaoca = rr.findByBrojRacuna(brojRacunaPosiljaoca);
 		if(racunPrimaoca.isPresent() && racunPosiljaoca.isPresent()) {
			Transakcija t = new Transakcija(null, racunPrimaoca.get(), racunPosiljaoca.get(), iznos);
			tr.save(t);
			return t;
		}
		
		return null;
	}
	
	public void removeTransakcija(Long id) {
		Optional<Transakcija> transakcija = tr.findById(id);
		tr.delete(transakcija.get());
	}
}
