package rs.ac.singidunum.banka;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import rs.ac.singidunum.banka.App;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=App.class)
@AutoConfigureMockMvc
public class RacuniControllerTest {
	@Autowired
	MockMvc mockMvc;
	
	@Test
	public void getRacuni() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/racuni").accept(MediaType.APPLICATION_JSON_UTF8))
						.andExpect(status().isOk())
						.andExpect(jsonPath("$", hasSize(5)));
	}
	
	@Test
	public void getRacun() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/racuni/551255112").accept(MediaType.APPLICATION_JSON_UTF8))
						.andExpect(jsonPath("$", notNullValue()))
						.andExpect(jsonPath("$.idRacuna", equalTo("551255112")));
	}
}
