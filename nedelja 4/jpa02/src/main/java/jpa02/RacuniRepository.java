package jpa02;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface RacuniRepository extends CrudRepository<Racun, Long>{
	
	public List<Racun> findByVlasnik(Student st);

}
