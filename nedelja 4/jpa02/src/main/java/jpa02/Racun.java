package jpa02;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Racun {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String opis;
	
	@ManyToOne
	@JsonIgnore
	private Student vlasnik;

	public Racun() {}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public Student getVlasnik() {
		return vlasnik;
	}
	public void setVlasnik(Student vlasnik) {
		this.vlasnik = vlasnik;
	}
	
	
	
	
}
