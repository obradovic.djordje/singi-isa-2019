package rs.ac.singidunum.banka.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rs.ac.singidunum.banka.model.Transakcija;
import rs.ac.singidunum.banka.services.TransakcijaService;

@RestController
@RequestMapping("/api/transakcije")
public class TransakcijaController {
	@Autowired
	TransakcijaService ts;

	@RequestMapping()
	public ResponseEntity<List<Transakcija>> dobavljanjeTransakcija() {
		return new ResponseEntity<List<Transakcija>>(ts.getTransakcije(), HttpStatus.OK);
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<Transakcija> dodavanjeTransakcije(@RequestBody Transakcija transakcija) {
		ts.addTransakcija(transakcija.getPosiljalac().getIdRacuna(), transakcija.getPrimalac().getIdRacuna(), transakcija.getIznos());
		return new ResponseEntity<Transakcija>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Transakcija> dobavljaneJedneTransakcije(@PathVariable String id) {
		Transakcija transakcija = ts.getTransakcija(id);
		if (transakcija != null) {
			return new ResponseEntity<Transakcija>(transakcija, HttpStatus.OK);
		}
		return new ResponseEntity<Transakcija>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Transakcija> uklanjanjeJednogRacuna(@PathVariable String id) {
		Transakcija transakcija = ts.getTransakcija(id);
		if (transakcija != null) {
			ts.removeTransakcija(transakcija);
		}
		return new ResponseEntity<Transakcija>(HttpStatus.NOT_FOUND);
	}
}
