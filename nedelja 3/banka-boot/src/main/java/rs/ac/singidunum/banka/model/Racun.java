package rs.ac.singidunum.banka.model;

public class Racun {
	private String idRacuna;
	private double stanje;
	
	public Racun() {
	}

	public Racun(String idRacuna, double stanje) {
		super();
		this.idRacuna = idRacuna;
		this.stanje = stanje;
	}

	public String getIdRacuna() {
		return idRacuna;
	}

	public void setIdRacuna(String idRacuna) {
		this.idRacuna = idRacuna;
	}

	public double getStanje() {
		return stanje;
	}

	public void setStanje(double stanje) {
		this.stanje = stanje;
	}
}
