package rs.ac.singidunum.banka.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.banka.model.Racun;
import rs.ac.singidunum.banka.model.Transakcija;

@Service
public class TransakcijaService {
	@Autowired
	RacunService rs;
	
	private ArrayList<Transakcija> transakcije;

	public TransakcijaService() {
		transakcije = new ArrayList<Transakcija>();
	}

	public ArrayList<Transakcija> getTransakcije() {
		return transakcije;
	}

	public Transakcija getTransakcija(String idTransakcije) {
		for (Transakcija t : transakcije) {
			if (t.getIdTransakcije().equals(idTransakcije)) {
				return t;
			}
		}

		return null;
	}

	public void addTransakcija(String posiljalacId, String primalacId, double iznos) {
		Racun posiljalac = rs.getRacun(primalacId);
		Racun primalac = rs.getRacun(primalacId);
		posiljalac.setStanje(posiljalac.getStanje() - iznos);
		primalac.setStanje(primalac.getStanje() + iznos);
		transakcije.add(new Transakcija(posiljalac.getIdRacuna() +"|" + primalac.getIdRacuna(), posiljalac, primalac, iznos));
	}
	
	public void removeTransakcija(Transakcija transakcija) {
		int i = 0;
		for(; i < transakcije.size(); i++) {
			if(transakcije.get(i).getIdTransakcije().equals(transakcija.getIdTransakcije())) {
				transakcije.remove(i);
				return;
			}
		}
	}
}
