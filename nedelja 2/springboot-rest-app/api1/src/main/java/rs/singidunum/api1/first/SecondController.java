package rs.singidunum.api1.first;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecondController {

	@Autowired
	private TopicService service;
	
	@RequestMapping("/topics")
	public List<Topic> pozdrav() {
		
		return service.all();
	}
	
	@RequestMapping("/topics/{id}")
	public Topic findTopic(@PathVariable String id) {
		return service.get(id);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/add")
	public void addTopic(@RequestBody Topic topic) {
		service.add(topic);
	}
	
}
