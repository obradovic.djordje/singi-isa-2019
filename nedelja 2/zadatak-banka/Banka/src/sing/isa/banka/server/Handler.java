package sing.isa.banka.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.springframework.context.ApplicationContext;

public class Handler implements Runnable {
	private Socket socket;
	private ApplicationContext context;

	public Handler(Socket socket, ApplicationContext context) {
		this.socket = socket;
		this.context = context;
	}

	@Override
	public void run() {
		try {
			PrintWriter os = new PrintWriter(socket.getOutputStream());
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			System.out.println("Konekcija pristigla od: " + socket.getInetAddress().toString());
			
			String input = in.readLine();
			String [] delovi = input.split(";");
			double iznos = Double.parseDouble(delovi[3]);
			
			Racun r1 = (Racun)context.getBean(delovi[1]);
			Racun r2 = (Racun)context.getBean(delovi[2]);
			
			r1.setRaspolozivo(r1.getRaspolozivo()-iznos);
			r2.setRaspolozivo(r2.getRaspolozivo()+iznos);
			
			os.println("Transakcija obavljena");
			os.close();
			in.close();
			socket.close();

			System.out.println("Stanje R1: " + r1.getRaspolozivo());
			System.out.println("Stanje R2: " + r2.getRaspolozivo());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
