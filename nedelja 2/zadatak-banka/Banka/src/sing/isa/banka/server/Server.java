package sing.isa.banka.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Server {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		
		int port = 3000;
		try {
			ServerSocket ss = new ServerSocket(port);
			System.out.println("Server slu�a na port: " + port);

			while (true) {
				Socket clientSocket = ss.accept();
				Handler h = new Handler(clientSocket, context);
				Thread t = new Thread(h);
				t.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Neuspe�no pokretanje servera.");
		}
	}
}
