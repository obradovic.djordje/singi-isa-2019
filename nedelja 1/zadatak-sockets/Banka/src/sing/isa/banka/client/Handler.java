package sing.isa.banka.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Handler implements Runnable {

	@Override
	public void run() {
		try {
			Socket clientSocket = new Socket("localhost", 3000);
			PrintWriter os = new PrintWriter(clientSocket.getOutputStream());
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

			for (int i = 0; i < 5; i++) {
				os.println("-100");
				os.flush();
				System.out.println(in.readLine());
			}
			
			for (int i = 0; i < 5; i++) {
				os.println("100");
				os.flush();
				System.out.println(in.readLine());
			}
			
			os.println("STOP");
			os.flush();

			os.close();
			in.close();
			clientSocket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			System.out.println("Zadat nepostojeći host.");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Neuspešno povezivanje na server.");
		}
	}

}
