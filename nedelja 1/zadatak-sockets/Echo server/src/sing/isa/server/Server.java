package sing.isa.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	public static void main(String[] args) throws InterruptedException {
		int port = 3000;
		try {
			ServerSocket ss = new ServerSocket(port);
			System.out.println("Server slu�a na port: " + port);

			while (true) {
				Socket clientSocket = ss.accept();
				PrintWriter os = new PrintWriter(clientSocket.getOutputStream());
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

				System.out.println("Konekcija pristigla od: " + clientSocket.getInetAddress().toString());
				System.out.println(in.readLine());
				os.println("PONG");
				os.flush();

				os.close();
				in.close();
				clientSocket.close();
				Thread.sleep(1000);
			}

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Neuspe�no pokretanje servera.");
		}
	}

}
