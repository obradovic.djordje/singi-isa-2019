package sing.isa.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
	public static void main(String[] args) {
		for (int i = 0; i < 100; i++) {
			try {
				Socket clientSocket = new Socket("localhost", 3000);
				PrintWriter os = new PrintWriter(clientSocket.getOutputStream());
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

				os.println("PING");
				os.flush();
				System.out.println(in.readLine());

				os.close();
				in.close();
				clientSocket.close();
			} catch (UnknownHostException e) {
				e.printStackTrace();
				System.out.println("Zadat nepostojeći host.");
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Neuspešno povezivanje na server.");
			}
		}
	}
}
