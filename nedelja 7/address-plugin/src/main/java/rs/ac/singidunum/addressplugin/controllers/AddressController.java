package rs.ac.singidunum.addressplugin.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.addressplugin.model.Address;

@Controller
@RequestMapping("/api/address")
public class AddressController {
	@RequestMapping
	public ResponseEntity<Address> getStuff() {
		return new ResponseEntity<Address>(new Address("ULICA", 55), HttpStatus.OK);
	}

}
