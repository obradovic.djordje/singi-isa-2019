package rs.ac.singidunum.addressplugin.model;

public class Address {
	private String ulica;
	private int broj;
	
	public Address() {
	}

	public Address(String ulica, int broj) {
		super();
		this.ulica = ulica;
		this.broj = broj;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public int getBroj() {
		return broj;
	}

	public void setBroj(int broj) {
		this.broj = broj;
	}
}
