package rs.ac.singidunum.host.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.host.plugin.PluginRepository;

@Controller
@RequestMapping("/api/address")
public class AddressController {
	@Autowired
	PluginRepository pr;
	
	@RequestMapping
	public ResponseEntity<String> getAddresses() {
		return pr.getPlugins("address").get(0).sendRequest(HttpMethod.GET, "http://localhost:8081/api/address");
	}
}
